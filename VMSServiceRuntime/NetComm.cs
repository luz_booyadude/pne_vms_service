﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace VMSServiceRuntime
{
    public class UDPService
    {
        //pinvoke
        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(uint DestIP, uint SrcIP, byte[] pMacAddr, ref int PhyAddrLen);


        public const long UDPBROADCASTADDR = 0xffffffff;
        public const int UDPLISTENINGPORT = 23452;

        private static Socket _s = null;
        private static Object _sendlock = new Object();
        private static Object _recvlock = new Object();

        public bool initError = false;

        public UDPService()
        {
            if (_s == null)
            {
                try
                {
                    _s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    _s.Bind(new IPEndPoint(0, UDPLISTENINGPORT));
                    _s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
                }
                catch (Exception)
                {
                    initError = true;
                    _s.Close();
                }                
            }
        }

        ~UDPService()
        {
            //shutdown socket connection
            if (_s != null)
            {
                _s.Close();
                _s = null;
            }
        }

        public void sendBroadcast(byte[] data, uint size)
        {
            lock(_sendlock)
            {
                if (_s == null)
                    throw new NullReferenceException();

                _s.SendTo(data, (int)size, 0, new IPEndPoint(UDPBROADCASTADDR, UDPLISTENINGPORT));
            }
        }

        public void send(string ipAddr, byte[] data, uint size, int port = UDPLISTENINGPORT)
        {
            lock (_sendlock)
            {
                if (_s == null)
                    throw new NullReferenceException();

                _s.SendTo(data, (int)size, 0, new IPEndPoint(IPAddress.Parse(ipAddr), port));
            }
        }

        public void recv(ref byte[] data, ref uint size, ref EndPoint ipinfo)
        {
            lock(_recvlock)
            {
                if (_s == null)
                    return;//throw new NullReferenceException();

                _s.ReceiveFrom(data, (int)size, 0, ref ipinfo);
            }
        }


        public string getMacAddr(string ipaddr)
        {
            IPAddress dst = IPAddress.Parse(ipaddr);

            uint uintAddress = BitConverter.ToUInt32(dst.GetAddressBytes(), 0);
            byte[] macAddr = new byte[6];
            int macAddrLen = macAddr.Length;
            int retValue = SendARP(uintAddress, 0, macAddr, ref macAddrLen);
            if (retValue == 0)
            {
                //string[] str = new string[(int)macAddrLen];
                string str = "";
                for (int i = 0; i < macAddrLen; i++)
                    str += macAddr[i].ToString("X2");
                return str;
            }

            return "";
        }
        
    }
}
