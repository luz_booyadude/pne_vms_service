﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;


namespace VMSServiceRuntime
{
    public partial class Runtime : ServiceBase
    {
        //import dll
        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        //constant and flag
        private bool _running = false;
        private const byte QUIT_FLAG = 0xff;
        public string masterIp { get; set; }
        private const string dir = @"C:\VMS\";
        private bool UpdateRunning = false;
        private string rootDir = AppDomain.CurrentDomain.BaseDirectory;

        //clase initialization
        UDPService udp = new UDPService();
        Thread udpProcessingThread;
        private System.Timers.Timer timer1 = null;

        public Runtime()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Library.WriteErrorLog("Root directory is set to " + rootDir);

            //start thread
            udpProcessingThread = new Thread(processUDPData);
            udpProcessingThread.Start();
            Library.WriteErrorLog("UDP thread started");

            //start timer
            timer1 = new System.Timers.Timer();
            this.timer1.Interval = 60000;
            this.timer1.Elapsed += new ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;
            Library.WriteErrorLog("Timer service started");
        }

        protected override void OnStop()
        {
            //kill udp thread
            _running = false;
            byte[] quit = { QUIT_FLAG };
            udp.send("127.0.0.1", quit, 1);
            udpProcessingThread.Abort();
            Library.WriteErrorLog("UDP thread stopped");

            //kill timer thread
            timer1.Enabled = false;
            Library.WriteErrorLog("Timer service stopped");
        }

        /// <summary>
        /// Check for VMS Service. If not exists, start it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            if (!UpdateRunning)
            {
                string ping = string.Empty;
                Process[] pname = Process.GetProcessesByName("VMSService");
                if (pname.Length == 0)
                {
                    try
                    {
                        Library.WriteErrorLog("Starting process...");
                        ProcessRun.ProcessAsUser.Launch(dir + @"VMSService.exe");
                        Thread.Sleep(2000);
                    }
                    catch (Exception) { }
                }
            }

        }

        public void processUDPData()
        {     
            byte[] rawdata = new byte[1024];
            uint size;
            EndPoint ipinfo = new IPEndPoint(0, 0);
            string data, ip;

            _running = true;

            Library.WriteErrorLog("UDP thread started successfully");

            while (_running)
            {
                Array.Clear(rawdata, 0, rawdata.Length);
                size = (uint)rawdata.Length;
                udp.recv(ref rawdata, ref size, ref ipinfo);

                if (rawdata[0] == QUIT_FLAG) break; //stop processing & quit upon receiving quit order

                ip = (ipinfo as IPEndPoint).Address.ToString();
                data = ASCIIEncoding.ASCII.GetString(rawdata, 0, (int)size);
                masterIp = ip;

                //received ping from control node
                if (data[0] == '+')
                {
                    string ping = "|+";
                    byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                    udp.send(masterIp, bytes, (uint)bytes.Length);
                    Library.WriteErrorLog("Ping Received");
                    continue;
                }  

                if(data.Substring(0, 7) == "|update")
                {
                    UpdateRunning = true;
                    Library.WriteErrorLog("Client software updated initialized");
                    //stop process
                    Process[] process = Process.GetProcessesByName("VMS_Client");
                    foreach (Process pr in process)
                    {
                        pr.Kill();
                    }
                    Thread.Sleep(2000);
                    process = Process.GetProcessesByName("VMSService");
                    foreach (Process pr in process)
                    {
                        pr.Kill();
                    }
                    Thread.Sleep(2000);

                    Process[] pname = Process.GetProcessesByName("VMS_Client");
                    if (pname.Length != 0)
                    {
                        continue;
                    }
                    Library.WriteErrorLog("VMS Client process stopped");

                    pname = Process.GetProcessesByName("VMSService");
                    if (pname.Length != 0)
                    {
                        continue;
                    }
                    Library.WriteErrorLog("VMS Listener process stopped");

                    ProcessRun.ProcessAsUser.Launch(rootDir + @"UpdateListener.bat");
                    //ProcessRun.ProcessAsUser.Launch(@"H:\OneDrive\PNE\GitLab\pne_vms_service\VMSServiceRuntime\bin\x86\Release\Update.bat");

                    ////delete from local folder first
                    //Library.WriteErrorLog("Deleting content of VMS folder...");
                    //string[] filePaths = Directory.GetFiles(dir);
                    //foreach(string filePath in filePaths)
                    //{
                    //    if(filePath.Contains("VMSService"))
                    //    {
                    //        File.Delete(filePath);
                    //    }
                    //}

                    ////copy new file from server
                    //Library.WriteErrorLog("Copying files from server...");
                    //foreach(var file in Directory.GetFiles(@"\\192.168.1.82\ClickOnce\VMS Listener\"))
                    //{
                    //    Library.WriteErrorLog(Path.Combine(dir, Path.GetFileName(file)));
                    //    File.Copy(file, Path.Combine(dir, Path.GetFileName(file)));
                        
                    //}

                    Library.WriteErrorLog("Client software updated.");
                    UpdateRunning = false;

                    continue;
                }
            }
        }
    }
}
